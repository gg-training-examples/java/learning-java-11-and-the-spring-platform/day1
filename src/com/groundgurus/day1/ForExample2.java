package com.groundgurus.day1;

public class ForExample2 {
  public static void main(String[] args) {
    // i = 2
    // j = 2
    // no of loops = 1, 2, 3, 4, 5, 6

    hoopla:
    for (int i = 1; i <= 10; i++) {
      for (int j = 1; j <= 5; j++) {
        if (i == 2 && j == 2) {
          break hoopla;
        } else {
          System.out.println("i = " + i + ", j = " + j);
        }
      }
    }
  }
}
