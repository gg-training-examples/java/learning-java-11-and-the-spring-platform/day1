package com.groundgurus.day1;

public class ForExample {
  public static void main(String[] args) {
    String days[] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

    for (var day : days) {
      System.out.println(day);
    }
  }
}
