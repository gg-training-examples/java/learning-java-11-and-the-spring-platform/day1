package com.groundgurus.day1;

public class VariableExamples {
  public static void main(String[] args) {
    var sum = 20 + 30;
    System.out.println(sum);

    // don't use byte and short, use int instead
    byte b = (byte) 129; // -2^7 (-128) to 2^7 - 1 (127) // 8 bits / 1 byte
    System.out.println(b); // -127

    int myArray[] = {10, 20, 30}; // 0, 1, 2 = 3
    System.out.println(myArray.length); // size of the array

    System.out.println(myArray[0]);

    //                     int[] 0    int[] 1    int[] 2
    int moreStuff[][] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} }; // int[][]
    System.out.println(moreStuff[1][1]);

    boolean anotherArray[] = new boolean[100]; // size = 100, default = false
    anotherArray[5] = true;
    System.out.println(anotherArray[5]);
  }

  public static void otherMethod() {
    var sum = 70;
  }
}
