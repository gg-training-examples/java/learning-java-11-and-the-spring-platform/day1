package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise3 {
  public static void main(String[] args) {
    int numbers[] = { 5, 7, 20, 42, 5, 10 };

    Scanner scanner = new Scanner(System.in);

    System.out.print("Enter a value: ");
    int value = scanner.nextInt();

    int count = 0;

    for (int i = 0; i < numbers.length; i++) {
      if (numbers[i] == value) {
        count++;
      }
    }

    if (count != 0) {
      System.out.println("There are " + count + " instances of the value "
        + value);
    } else {
      System.out.println("-1");
    }

    scanner.close();
  }
}
