package com.groundgurus.day1.exercises;

import java.io.File;
import java.net.URISyntaxException;

public class Exercise5 {
  public static void main(String[] args) throws URISyntaxException {
//    Properties props = System.getProperties();
//    props.list(System.out);
    String path = System.getProperty("user.home")
      + File.separator + "chromedriver";

    File myFile = new File(path);

    System.out.println("Hidden: " + myFile.isHidden());
    System.out.println("Directory: " + myFile.isDirectory());
    System.out.println("File: " + myFile.isFile());
    System.out.println("Absolute Path: " + myFile.getAbsolutePath());
  }
}
