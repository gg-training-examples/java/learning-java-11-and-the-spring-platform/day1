package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise4 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Enter a number: ");
    int number = scanner.nextInt();

    for (int i = 1; i <= number; i++) {
      for (int j = 1; j <= number; j++) {
        System.out.print("*");

        // enter a space if not at the end of x axis
        if (j != number) {
          System.out.print(" ");
        }
      }
      System.out.println();
    }

    scanner.close();
  }
}
