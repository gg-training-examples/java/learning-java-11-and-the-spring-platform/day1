package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise1 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Enter the temperature (Celsius): ");
    double celsius = scanner.nextDouble();

    // convert celsius into fahrenheit
    double fahrenheit = (celsius * 9/5) + 32;

    // convert celsius into kelvin
    double kelvin = celsius + 273.15;

    System.out.println("The value in Fahrenheit is " + fahrenheit);
    System.out.println("The value in Kelvin is " + kelvin);

    scanner.close();
  }
}
