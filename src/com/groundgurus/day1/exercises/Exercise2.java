package com.groundgurus.day1.exercises;

import java.util.Scanner;

public class Exercise2 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Enter the string: ");
    String message = scanner.next();
    String output = "";

    for (int i = 0; i < message.length(); i++) {
      String str = message.charAt(i) + "";

      if (i % 2 == 0) {
        output += str.toLowerCase();
      } else {
        output += str.toUpperCase();
      }
    }

    System.out.println(output);

    scanner.close();
  }
}
