package com.groundgurus.day1;

/**
 * <h1>Hello World</h1>
 * <p>
 * This is an example class for operators
 * </p>
 * @author pgutierrez
 * @version 1.0.0
 */
public class OperatorExample {
  public static void main(String[] args) {
    var x = 10;
    var y = 20;
    x++; // 11
    y++; // 21
    //      11  + 13  + 21
    var z = x++ + ++x + y++;

    System.out.println("x = " + x);
    System.out.println("y = " + y);
    System.out.println("z = " + z);

    var a = 20; // 0001 0100
    var b = a << 2; //    0001 0100
                    // <<         2
                    // ------------
                    //    0101 0000 = 64 + 16
    System.out.println("b = " + b);

    var c = a ^ 2; //    0001 0100
                   // ^  0000 0010
                   // ------------
                   //    0001 0110 = 16 + 4 + 2

    //c = c + 10;
    c += 10;
    System.out.println("c = " + c);
    var d = (10 + 20.0) / 30 * 40;
    //  d = 30.0 / 30 * 40
    //  d = 1.0 * 40
    //  d = 40.0
    System.out.println("d = " + d);

//    int total = 10 + 20 + 30 + 40 + 50;
    String resultMessage = "You have a total of " + 10 + 20 + 30 + 40 + 50;
    //                     "You have a total of 10" + 20 + 30 + 40 + 50
    //                     "You have a total of 1020" + 30 + 40 + 50
    // and so on and so forth...
    System.out.println(resultMessage);
  }
}
