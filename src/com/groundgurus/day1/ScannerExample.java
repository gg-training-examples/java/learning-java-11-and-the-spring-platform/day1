package com.groundgurus.day1;

import java.util.Scanner;

public class ScannerExample {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Your Name Please: ");
    String input = scanner.nextLine();

    System.out.printf("Hello %s%n", input);

    scanner.close();
  }
}
